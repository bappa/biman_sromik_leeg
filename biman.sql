-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2016 at 02:05 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biman`
--

-- --------------------------------------------------------

--
-- Table structure for table `labor`
--

CREATE TABLE `labor` (
  `id` int(10) UNSIGNED NOT NULL,
  `lb_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designetion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blood_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guardian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gardian_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `id_card_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `division` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pg_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `another_uni_member` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_union_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `membership_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `join_date` date NOT NULL,
  `present_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `persent_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parmanent_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parmanent_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `labor`
--

INSERT INTO `labor` (`id`, `lb_id`, `image`, `name`, `designetion`, `email_id`, `blood_group`, `guardian`, `gardian_name`, `date_of_birth`, `id_card_number`, `division`, `pg_no`, `another_uni_member`, `other_union_name`, `membership_type`, `join_date`, `present_address`, `persent_mobile`, `parmanent_address`, `parmanent_mobile`, `created_at`, `updated_at`) VALUES
(1, '1', 'bappa.jpg', 'বাপ্পা সরকার', 'ম্যানেজার ', '', 'O +', 'father', 'সুজন কুমার সরকার ', '2016-02-03', '123456789', 'বিভাগ ', '12345678', '0', NULL, 'parmanent', '2016-02-10', 'ঠিকানা ', '0987654321', 'ঠিকানা ', '987654321', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '2', 'mita.jpg', 'jhkjhk', 'hjhkh', 'sfsdgfdsg@rgftuh.hjg', 'A -', 'father', 'erteryr', '2016-02-04', '1234356789', 'sdfghjk', '23456789', '0', NULL, 'parmanent', '2016-02-08', 'sdrfghjkhj', '435678', 'wertyu', '435678', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `labor_info`
--

CREATE TABLE `labor_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_01_13_101238_create_users_table', 1),
('2016_01_13_111025_create_labor_table', 1),
('2016_01_15_105616_create_labor_info_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `profile_image`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bappa', 'Sarker', 'admin', 'admin@biman.com', '$2y$10$4KrGEFEYq1tXbnzhEUReZOWf0JxSykuAUkDeQ6aDKxsd4mDiM24Xa', 'Untitled-1.png', 'Mirpur 10', NULL, '2016-02-01 06:03:21', '2016-02-01 06:03:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `labor`
--
ALTER TABLE `labor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labor_info`
--
ALTER TABLE `labor_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `labor`
--
ALTER TABLE `labor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `labor_info`
--
ALTER TABLE `labor_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
