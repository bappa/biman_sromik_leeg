<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// User managment route
Route::get('/', array('as' => 'admin', 'uses' => 'UsersController@adminLoginForm'));  
Route::post('/login', array('as' => 'login', 'uses' => 'UsersController@adminLoginCheck'));
Route::get('/adminDashboard', array('as' => 'adminDashboard', 'uses' => 'UsersController@adminDashboard')); 
Route::get('/logout', array('as' => 'logout', 'uses' => 'UsersController@getLogOut')); 
Route::get('/updateUserProfileForm', array('as' => 'updateUserProfileForm', 'uses' => 'UsersController@updateUserProfileForm'));
Route::post('/updateUserProfile', array('as' => 'updateUserProfile', 'uses' => 'UsersController@updateUserProfile')); 

//Labor Manegment
Route::get('/ctrateLaborBioForm', array('as' => 'ctrateLaborBioForm', 'uses' => 'LaborController@ctrateLaborBioForm')); 
Route::post('/ctrateLaborBioSaveForm', array('as' => 'ctrateLaborBioSaveForm', 'uses' => 'LaborController@ctrateLaborBioSaveForm')); 
Route::get('/laborListView', array('as' => 'laborListView', 'uses' => 'LaborController@laborListView')); 
Route::get('/laborDelete/{id}', array('as' => 'laborDelete', 'uses' => 'LaborController@laborDelete')); 
Route::any('/laborUpdateForm/{id}', array('as' => 'laborUpdateForm', 'uses' => 'LaborController@laborUpdateForm')); 
Route::post('/updateLaborBioSaveForm', array('as' => 'updateLaborBioSaveForm', 'uses' => 'LaborController@updateLaborBioSaveForm')); 
Route::get('/laborBioView/{id}', array('as' => 'laborBioView', 'uses' => 'LaborController@laborView')); 



