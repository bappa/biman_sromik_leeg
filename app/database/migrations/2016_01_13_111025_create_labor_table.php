<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaborTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('labor', function(Blueprint $table) {
            $table->increments('id');
            $table->string('lb_id');
            $table->string('image');
            $table->string('name');
            $table->string('designetion');
            $table->string('email_id');
            $table->string('blood_group');
            $table->string('guardian');
            $table->string('gardian_name');
            $table->date('date_of_birth');
            $table->string('id_card_number');
            $table->string('division');
            $table->string('pg_no');
            $table->string('another_uni_member');
            $table->string('other_union_name')->nullable();
            $table->string('membership_type');
            $table->date('join_date');
            $table->string('present_address');
            $table->string('persent_mobile');
            $table->string('parmanent_address');
            $table->string('parmanent_mobile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('labor');
    }

}
