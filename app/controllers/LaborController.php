<?php
class LaborController extends BaseController {

       public function ctrateLaborBioForm() {
        $data = array(
            'title' => 'Create Labour Biography',
            'last_id' => Labor::getLabour(),
        );
        return View::make('admin.pages.labor_bio_form')->with($data);
    }
    
    public function ctrateLaborBioSaveForm(){
         $validation_rule = array(
            'name' => array('required','max:100'),
            'designetion' => array('required', 'max:1000'),
            'blood_group' => array('required','max:100'),
            'email_id' => array('required', 'unique:labor','email'),
            'guardian' => array('required'),
            'guardian_name' => array('required', 'max:500'),
            'date_of_birth' => array('required'),
            'id_card_number' => array('required'),
            'division' => array('required', 'max:500'),
            'pg_no' => array('required'),
            'other_union_name' => array( 'required_if:another_uni_member,1', 'max:500'),
            'membership_type' => array('required', 'max:500'),
            'join_date' => array('required', 'max:500'),
            'present_address' => array('required', 'max:500'),
            'persent_mobile' => array('required', 'max:15'),
            'parmanent_address' => array('required', 'max:500'),
            'parmanent_mobile' => array('required', 'max:15'),
        );
        $validation = Validator::make(Input::all(), $validation_rule);
        if ($validation->fails()) {
            return Redirect::to('/ctrateLaborBioForm')->withErrors($validation)->withInput();
        }  else {
                   if (Input::hasFile('image')) {
                $input = array('image' => Input::file('image'));
                $rules = array(
                    'image' => 'image'
                );
                $validator = Validator::make($input, $rules);
                if ($validator->fails()) {
                    return Redirect::to('/ctrateLaborBioForm')->with('image_validation_error', 'Image file is not valid !');
                } else {
                    //Upload the image and get the image file name. 
                    $image = Input::file('image');
                    $destinationPath = 'public/uploads/labor';
                    $filename = $image->getClientOriginalName();
                    Input::file('image')->move($destinationPath, $filename);
                }
            } else {
                // Get the old image name
                $filename = '';
            }
            
            $id = DB::table('labor')->insertGetId(
                    array(
                        'lb_id' => Input::get('lb_id'),
                        'name' => Input::get('name'),
                        'blood_group' => Input::get('blood_group'),
                        'email_id' => Input::get('email_id'),
                        'image' => $filename,
                        'guardian' => Input::get('guardian'),
                        'designetion' => Input::get('designetion'),
                        'gardian_name' => Input::get('guardian_name'),
                        'date_of_birth' => Input::get('date_of_birth'),
                        'id_card_number' => Input::get('id_card_number'),
                        'division' => Input::get('division'),
                        'pg_no' => Input::get('pg_no'),
                        'another_uni_member' => Input::get('another_uni_member'),
                        'other_union_name' => Input::get('other_union_name'),
                        'membership_type' => Input::get('membership_type'),
                        'join_date' => Input::get('join_date'),
                        'present_address' => Input::get('present_address'),
                        'persent_mobile' => Input::get('persent_mobile'),
                        'parmanent_address' => Input::get('parmanent_address'),
                        'parmanent_mobile' => Input::get('parmanent_mobile'),
            ));
          return Redirect::to('/ctrateLaborBioForm')->with('add_success_massege', 'Added Successfully !');
        }
    }
    
    public function laborListView(){
        $data = array(
            'title' => 'Labour List View',
            'labor_list' => Labor::getAllLabor()
        );
        return View::make('admin.pages.labor_list_view')->with($data);
    } 
    
    public function laborDelete($id){
       Labor::deletLabor($id);
       return Redirect::to('/laborListView')->with('delete_success_massege', 'Deleted Successfully !');
    }
    
    public function laborUpdateForm($id){
        $data = array(
            'title' => 'Edit Labor Form',
            'labor_info' => Labor::getLabourInfoById($id),
        );
        return View::make('admin.pages.labor_bio_edit_form')->with($data);
    }
    
    public function updateLaborBioSaveForm(){
        $id =Input::get('id');
          $validation_rule = array(
            'name' => array('required','max:100'),
            'blood_group' => array('required','max:100'),
            'email_id' => array( 'unique:labor','email'),
            'designetion' => array('required', 'max:1000'),
            'guardian' => array('required'),
            'guardian_name' => array('required', 'max:500'),
            'date_of_birth' => array('required'),
            'id_card_number' => array('required'),
            'division' => array('required', 'max:500'),
            'pg_no' => array('required'),
            'other_union_name' => array( 'required_if:another_uni_member,1', 'max:500'),
            'membership_type' => array('required', 'max:500'),
            'join_date' => array('required', 'max:500'),
            'present_address' => array('required', 'max:500'),
            'persent_mobile' => array('required', 'max:15'),
            'parmanent_address' => array('required', 'max:500'),
            'parmanent_mobile' => array('required', 'max:15'),
        );
        $validation = Validator::make(Input::all(), $validation_rule);
        if ($validation->fails()) {
//            echo'validation fails';exit;
            return Redirect::to('/laborUpdateForm/'.$id)->withErrors($validation)->withInput();
        }  else {
                   if (Input::hasFile('image')) {
                $input = array('image' => Input::file('image'));
                $rules = array(
                    'image' => 'image'
                );
                $validator = Validator::make($input, $rules);
                if ($validator->fails()) {
                    return Redirect::to('/laborUpdateForm/'.$id)->with('image_validation_error', 'Image file is not valid !');
                } else {
                    //Upload the image and get the image file name. 
                    $image = Input::file('image');
                    $destinationPath = 'public/uploads/labor';
                    $filename = $image->getClientOriginalName();
                    Input::file('image')->move($destinationPath, $filename);
                }
            } else {
                // Get the old image name
                $filename = Input::get('image');
            }
            
            
            DB::table('labor')
                ->where('id', $id)
                ->update([
                        'lb_id' => Input::get('lb_id'),
                        'name' => Input::get('name'),
                        'blood_group' => Input::get('blood_group'),
                        'email_id' => Input::get('email_id'),
                        'image' => $filename,
                        'guardian' => Input::get('guardian'),
                        'designetion' => Input::get('designetion'),
                        'gardian_name' => Input::get('guardian_name'),
                        'date_of_birth' => Input::get('date_of_birth'),
                        'id_card_number' => Input::get('id_card_number'),
                        'division' => Input::get('division'),
                        'pg_no' => Input::get('pg_no'),
                        'another_uni_member' => Input::get('another_uni_member'),
                        'other_union_name' => Input::get('other_union_name'),
                        'membership_type' => Input::get('membership_type'),
                        'join_date' => Input::get('join_date'),
                        'present_address' => Input::get('present_address'),
                        'persent_mobile' => Input::get('persent_mobile'),
                        'parmanent_address' => Input::get('parmanent_address'),
                        'parmanent_mobile' => Input::get('parmanent_mobile'),
        ]);
            
          return Redirect::to('/laborListView')->with('update_success_massege', 'Updated Successfully !');
        }
    }
    
    
    public function laborView($id){
        
      $data = array(
            'title' => 'Edit Labor Form',
            'labor_info' => Labor::getLabourInfoById($id),
        );
       return View::make('admin.pages.view_labor_page')->with($data);
    }
    

}