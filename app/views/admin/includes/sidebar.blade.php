<!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{URL::to('public/uploads/profile/'.Auth::user()->profile_image)}}" class="img-circle" alt="User Image">
            </div>
          </div>
          <!-- search form -->
        
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="<?php echo URL::route('adminDashboard')?>">
                <i class="fa fa-dashboard"></i> <span>ড্যাশবোর্ড</span> 
              </a>
            </li>
            <li><a href="<?php echo URL::route('ctrateLaborBioForm') ?>"><i class="fa fa-user-plus text-green"></i> <span>সদস্য যুক্ত করুন</span></a></li>
            <li><a href="<?php echo URL::route('laborListView') ?>"><i class="fa fa-list"></i> <span> সকল সদস্যের তালিকা</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->