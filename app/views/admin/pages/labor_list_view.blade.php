@extends('admin.templates.default')
@section('content')
<section class="content">
     <?php if (Session::get('delete_success_massege')) { ?>
        <div class="bs-example col-md-9">
            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo Session::get('delete_success_massege'); ?>
            </div>
        </div>
    <?php } ?>
    <?php if (Session::get('update_success_massege')) { ?>
        <div class="bs-example col-md-9">
            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo Session::get('update_success_massege'); ?>
            </div>
        </div>
    <?php } ?>
      <!-- /////////////          Display  all products in stock         ////////////  -->
      <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"> সকল সদস্য </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>সদস্য নাম্বার</th>
                                <th>ছবি</th>
                                <th>সদস্যের নাম </th>
                                <th>পদবী</th>
                                <th>সদস্যের ধরন</th>
                                <th>বিভাগ</th>
                                <th>মোবাইল</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($labor_list)){
                            foreach ($labor_list as $single_labor) {
                                ?>
                                <tr>
                                    <td> <?php echo $single_labor->lb_id; ?></td>
                                    <td> <img src="<?php echo 'public/uploads/labor/'.$single_labor->image?>" height="100" width="100"></td>
                                    <td> <?php  echo $single_labor->name; ?></td>
                                    <td> <?php  echo $single_labor->designetion; ?></td>
                                    <td> <?php  
                                    if($single_labor->membership_type === 'parmanent'){
                                        echo ' স্থায়ী'; 
                                    }else{
                                         echo ' অস্থায়ী'; 
                                    }
                                    ?>
                                    </td>
                                    <td> <?php  echo $single_labor->division?> </td>
                                    <td> <?php  echo $single_labor->persent_mobile?> </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary ">Action</button>
                                            <button type="button" class="btn btn-primary  dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo URL::action('LaborController@laborView',[$single_labor->id]) ?>" target="_blank">View</a></li>
                                                <li><a href="<?php echo URL::action('LaborController@laborUpdateForm',  [$single_labor->id]) ?>">Edit</a></li>
                                                <li><a href="<?php echo URL::action('LaborController@laborDelete',[$single_labor->id]) ?>">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php }} ?>
                                
                        </tbody>
                        <tfoot>
                            
                            <tr>
                              <th>সদস্য নাম্বার</th>
                                <th>ছবি</th>
                                <th>সদস্যের নাম </th>
                                <th>পদবী</th>
                                <th>সদস্যের ধরন</th>
                                <th>বিভাগ</th>
                                <th>মোবাইল</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div>
 </section><!-- /.content -->
@stop