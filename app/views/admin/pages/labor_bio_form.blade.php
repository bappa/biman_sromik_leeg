@extends('admin.templates.default')
@section('content')
<section class="content">
    <?php if (Session::get('add_success_massege')) { ?>
        <div class="bs-example col-md-9">
            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo Session::get('add_success_massege'); ?>
            </div>
        </div>
    <?php } ?>
    <div class="row">

        <!-- right column -->
        <div class="col-md-11">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <i class="fa fa-user-plus text-green"></i> 
                    <h3 class="box-title"> নতুন সদস্য যুক্ত করুন </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo Form::open(array('route' => 'ctrateLaborBioSaveForm', 'files' => true, 'class' => 'form-horizontal')) ?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-9">
                          <?php  echo Form::hidden('lb_id', $value = ($last_id+1), $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">নামঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::text('name', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('name'); ?></span>
                        </div>
                        <label id="inputSuccess" class="col-sm-3 control-label">পদবীঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::text('designetion', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('designetion'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                          <label id="inputSuccess" class="col-sm-3 control-label">রক্তের গ্রুপঃ <b class="mandetory_star">*</b></label>
                          <div class="col-sm-3">
                              <?php
                              echo Form::select('blood_group', array(
                                  'A +' => 'A +',
                                  'A -' => 'A -',
                                  'B +' => 'B +',
                                  'B -' => 'B -',
                                  'O +' => 'O +',
                                  'O -' => 'O -',
                                  'O -' => 'O -',
                                  'O -' => 'O -',
                              ));
                              ?>
                              <span class="text-red"><?php echo $errors->first('blood_group'); ?></span>
                          </div>
                        <label id="inputSuccess" class="col-sm-3 control-label">ইমেইল আইডিঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::email('email_id', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('email_id'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">পিতা/স্বামী <b class="mandetory_star">*</b></label>
                        <div class="col-sm-4">
                                <div class="radio">
                                    <label>
                                        <?php  echo Form::radio('guardian', 'father','checked');?>
                                         পিতা
                                          <?php  echo Form::radio('guardian', 'husband');?>
                                          স্বামী 
                                    </label>
                                </div>
                               <span class="text-red"><?php echo $errors->first('guardian'); ?></span>
                            </div>
                        <label id="inputSuccess" class="col-sm-2 control-label">পিতা/স্বামীর নামঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::text('guardian_name', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('guardian_name'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">জন্ম তারিখঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::input('date', 'date_of_birth',null ,$attributes = array('class' => 'form-control')); ?>
                            <span class="text-red"><?php echo $errors->first('date_of_birth'); ?></span>
                        </div>
                        <label id="inputSuccess" class="col-sm-3 control-label">পরিচয় পত্রের নাম্বারঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::number('id_card_number', '', $attributes = array('class' => 'form-control','id' =>'membership_type' ,'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('id_card_number'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">বিভাগঃ<b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::text('division', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('division'); ?></span>
                        </div>
                        <label id="inputSuccess" class="col-sm-3 control-label">পি / জি নাম্বারঃ <b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::number('pg_no', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('pg_no'); ?></span>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">অন্য কোন ট্রেড ইউনিয়নের সদস্য কিনাঃ<b class="mandetory_star">*</b></label>
                         <div class="col-sm-2">
                            <div class="checkbox" >
                                <label class="another_unionmember_no">
                                    <?php echo Form::radio('another_uni_member',0,'true',['id' => 'another_unionmember_no']);?>
                                   না 
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="checkbox" >
                                <label>
                                    <?php echo Form::radio('another_uni_member',1,'',['id' => 'another_unionmember_yes']);?>
                                    হ্যাঁ 
                                </label>
                            </div>
                        </div>
                       
                        <label id="inputSuccess" class="col-sm-2 control-label">ইউনিয়নের নামঃ</label>
                        <div class="col-sm-3" id= "union_activet">
                            <?php echo Form::text('other_union_name', $value = ' ', $attributes = array('class' => 'form-control', 'id' =>'other_union_name','disabled','placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('other_union_name'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">সদস্য পদের ধরনঃ<b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <div class="radio">
                                    <label>
                                        <?php  echo Form::radio('membership_type', 'parmanent','checked');?>
                                         স্থায়ী
                                         <?php  echo Form::radio('membership_type', 'temporary');?>
                                         অস্থায়ী 
                                    </label>
                            </div>
                             <span class="text-red"><?php echo $errors->first('membership_type'); ?></span>

                        </div>
                        <label id="inputSuccess" class="col-sm-3 control-label">যোগদানের তারিখঃ<b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::input('date', 'join_date',null ,$attributes = array('class' => 'form-control')); ?>
                            <span class="text-red"><?php echo $errors->first('join_date'); ?></span>
                        </div>
                    </div>
                    
                   
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">বর্তমান ঠিকানাঃ  <b class="mandetory_star">*</b> </label>
                        <div class="col-sm-9">
                            <?php echo Form::textarea('present_address', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('present_address'); ?></span>
                        </div>
                    </div>
                     <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">বর্তমান ফোন/ মবাইল নাম্বারঃ<b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::number('persent_mobile', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('persent_mobile'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">স্থায়ী ঠিকানাঃ<b class="mandetory_star">*</b> </label>
                        <div class="col-sm-9">
                            <?php echo Form::textarea('parmanent_address', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('parmanent_address'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label id="inputSuccess" class="col-sm-3 control-label">স্থায়ী ফোন/ মোবাইল নাম্বারঃ<b class="mandetory_star">*</b></label>
                        <div class="col-sm-3">
                            <?php echo Form::number('parmanent_mobile', '', $attributes = array('class' => 'form-control', 'placeholder' => '')); ?>
                            <span class="text-red"><?php echo $errors->first('parmanent_mobile'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                         <div class="col-md-3 form-level"><label>সদস্যের ছবিঃ </label></div>
                         <div class="col-md-7">  
                             <?php echo Form::file('image');?>
                             <span class="text-red"><?php  echo  Session::get('image_validation_error'); ?></span>
                         </div>
                     </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <?php echo Form::submit('Submit', array('class' => 'btn btn-info pull-right inside_body_submit')) ?>
                </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
            <?php echo Form::close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->
@stop
