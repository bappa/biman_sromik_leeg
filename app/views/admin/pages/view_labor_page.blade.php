@extends('admin.templates.view')
@section('content')
<style>
    .heading{
        text-align: center;
        font-size: 18px;
        line-height: 20px;
    }
/*    .align_middle{
      text-align: center;  
      font-size: 20px;
    }*/
    .main_heading{
        font-size: 40px;
    }
    .container{
        border: 1px solid #ddd;
        padding-top: 25px;
        padding-bottom: 15px;
        margin-bottom: 20px;
        font-size: 13px;
    }
    .middle_align{
        text-align:center;
    }
    .right_align{
        float: right;
    }
    .right_align_chirman{
          float: right;
    }
</style>
        <?php foreach ($labor_info as $single_info) { ?>
        <div class="row heading">
                <div class="col-md-2 right_align">  <img src="{{URL::to('public/uploads/labor/'.$single_info->image)}}" height="100" width="100" alt="User Image"></div>
                <div class="col-md-8 middle_align">
                    ফর্ম "ঘ"<br><br>
                    <p class="main_heading"> <b>বিমান শ্রমিক লীগ </b> </p>
                    <p><b>রেজিঃ নং : বি -২০২৫</b> </p>
                    <p><b>কেন্দ্রীয় কমিটি</b>  </p>
                    <p>প্রধান কার্যালয়ঃ হযরত শাহজালাল (রহঃ) আন্তর্জাতিক বিমান বন্দর, কুর্মিটোলা, ঢাকা-১২২৯।</p>
                </div>
                <div class="col-md-2 right_align">
                  
                </div>
          </div>

         <div class="row">
                <div class="col-md-8">
                    <addrsss>
                        বরাবর,<br>
                               সাধারন সম্পাদক ,<br>
                               বাংলাদেশ বিমান শ্রমিক লীগ, <br>
                               কেন্দ্রীয় কমিটি, ঢাকা। <br>
                    </addrsss>
                </div><!-- End of address-->
                <div class="col-md-4 right_align">
                    সদস্য নং : <?php echo $single_info->lb_id; ?>
                </div>
         </div> <!-- End ofthe row-->
         <div class="row">
                 <div class="col-md-2"></div>
                 <div class="col-md-8 align_middle">
                      <b>বিষয়: সদস্য পদ লাভের জন্য আবেদন।</b><br><br>
                 </div>
                 <div class="col-md-2"></div>
             </div><!-- End of the row-->

         <div class="row">
                <div class="col-md-12">
                    <p>
                        জনাব,
                        আমি <?php echo $single_info->name; ?> . ট্রেড ইউনিয়নের সদস্য পদ লাভের জন্য এতদ্বারা আবেদন করিতেছি।<br>
                        আমি মনোযোগের সাথে বাংলাদেশের বিমান শ্রমিক লীগের গঠনতন্ত্রের বিধান সমূহ পড়েছি এবং এসব নিয়ম মেনে চলতে প্রস্তুত আছি। আমার সম্পর্কে প্রয়োজনীয় তথ্য দেওয়া হল।  
                    </p>
                </div>
         </div>

         <div class="row">
                <div class="col-md-8">
                    ১। নাম: <?php echo $single_info->name; ?> ,পদবী:  <?php echo $single_info->designetion ?>,
                    রক্তের গ্রুপঃ <?php echo $single_info->blood_group; ?> , ইমেইল:<?php echo $single_info->email_id ?><br>
                    <?php if ($single_info->guardian === 'father') { ?>
                     ২। <?php echo 'পিতা'; ?>: <?php echo $single_info->gardian_name; ?><br>
                    <?php } else { ?>
                     ২।  <?php echo 'স্বামী'; ?>: <?php echo $single_info->gardian_name; ?><br>
                    <?php } ?>
                    ৩। জন্ম তারিখ : <?php echo $single_info->date_of_birth; ?><br>
                    ৪। কর্মরত প্রতিষ্ঠানের নামঃ বিমান বাংলাদেশ এয়ারলাইন্স .<br> 
                    ৫। বিভাগ: <?php echo $single_info->division ?> , পি/জি নং: <?php echo $single_info->pg_no; ?><br>
                    ৬। পরিচয় পত্র নং: <?php echo $single_info->id_card_number; ?> <br>
                    <?php if ($single_info->another_uni_member === 1) { ?>
                    ৭।  আমি <?php echo $single_info->other_union_name; ?>  ট্রেড ইউনিয়নের সদস্য।<br>
                    <?php } else { ?>
                    ৭। আমি অন্য কোন ট্রেড ইউনিয়নের সদস্য না । <br>      
                    <?php } ?>
                    ৮। সদস্য পদের ধরন: 
                        <?php if($single_info->membership_type === 'temporary'){?>
                            অস্থায়ী
                       <?php }  else {?>
                                   স্থায়ী   
                      <?php  }?>
                    <br>
                    ৯। বর্তমান চাকুরিতে যোগদানের তারিখঃ <?php echo $single_info->join_date ?><br><br><br>
                   ১০। বর্তমান ঠিকানা:<address><?php echo $single_info->present_address ?>.</address> 
                   বর্তমান  মোবাইল: <?php echo $single_info->persent_mobile; ?>.<br><br><br>
                    ১১। স্থায়ী ঠিকানা:<address><?php echo $single_info->parmanent_address; ?>.</address> স্থায়ী মোবাইল: <?php echo $single_info->parmanent_mobile; ?>.<br>

                   <br><p> আমি এতদ্বারা ঘোষণা করছি যে, আমি বাংলাদেশ বিমানের অন্য কোন ইউনিয়নের সদস্য নহি । </p>    
                </div>
               <div class="col-md-4"> 
                 </div>
         </div>
          <div class="row">
              <div class="col-md-8"></div>
                 <div class="col-md-4 right_align"> 
                     <p>স্বাক্ষর/ টিপ সহি </p>
                     <p>তারিখঃ.....................................................................</p>
                 </div>
           </div>
             

            <hr>

            <div class="row heading">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                      ফর্ম "ঘ"<br><br>
                    <p class="main_heading"> <b>বিমান শ্রমিক লীগ </b> </p>
                    <p><b>রেজিঃ নং : বি -২০২৫</b> </p>
                    <p><b>কেন্দ্রীয় কমিটি</b>  </p>
                    <p>প্রধান কার্যালয়ঃ হযরত শাহজালাল (রহঃ) আন্তর্জাতিক বিমান বন্দর, কুর্মিটোলা, ঢাকা-১২২৯</p>
                </div>
                <div class="col-md-2"></div>
          </div>

         <div class="row">
                <div class="col-md-12">
                    <p> জনাব  <?php echo $single_info->name; ?> , পদবী: <?php echo $single_info->designetion ?>, 
                    </p>
                     পি/জি : <?php echo $single_info->pg_no; ?>. আপনার আবেদন বিবেচনা পূর্বক গৃহীত হলো / হলোনা ।
                </div> 
         </div><br><br>
         <div class="row">
                <div class="col-md-6">

                    ........................................................<br>
                 (সাধারন সম্পাদক)
                </div>
                <div class="col-md-6 right_align ">
                    ........................................................<br>
                   ( সভাপতি)
                </div>
         </div>

        <?php } ?>
@stop




