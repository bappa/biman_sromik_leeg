<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       @include('admin.includes.head')
  </head>
  <body onload="window.print()">
      <!-- Content Wrapper. Contains page content -->
      <div class="container" style="margin-top:5px;">
          @yield('content')
      </div><!-- /.content-wrapper -->
          @include('admin.includes.foot')
  </body>
</html>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

